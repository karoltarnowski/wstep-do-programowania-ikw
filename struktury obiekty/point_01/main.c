/* point_01
Program definujący strukturę point
i funkcje obsługujące tę strukturę.
Program demonstruje możliwości
jej wykorzystania.
*/

#include <stdio.h>
#include <math.h>

struct point{
    double x,y;
};

typedef struct point point;

int    printPoint(point*);
double modulus(point*);
point  rotated(point*, double);

int main()
{
    point a = {3,4}, b;
    printf("a = ");printPoint(&a);printf("\n");
    printf("a_x = %lf\n",a.x);
    printf("a_y = %lf\n",a.y);
    printf("|a| = %lf\n",modulus(&a));
    b = rotated(&a,M_PI_2);
    printf("b = ");printPoint(&b);printf("\n");
    return 0;
}

int printPoint(point*a){
    return printf("[%lf,%lf]",a->x,a->y);
}

double modulus(point*a){
    return sqrt(a->x*a->x + a->y*a->y);
}

point rotated(point*a, double alpha){
    point b;
    b.x = a->x*cos(alpha) - a->y*sin(alpha);
    b.y = a->x*sin(alpha) + a->y*cos(alpha);
    return b;
}











