#ifndef POINT_H
#define POINT_H

class point
{
    public:
        point();
        point(double,double);
        void print();
        double modulus();
        point rotated(double alpha);
        double getX();
        double getY();

    private:
        double _x;
        double _y;
};

#endif // POINT_H
