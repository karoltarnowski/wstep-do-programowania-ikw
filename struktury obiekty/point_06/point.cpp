#include "point.h"
#include <iostream>
#undef __STRICT_ANSI__
#include <cmath>

using namespace std;

point::point(){
    _x = 0;
    _y = 0;
}

point::point(double x, double y){
    _x = x;
    _y = y;
}

void point::print(){
    cout << "[" << _x << "," << _y << "]";
}

double point::modulus(){
    return sqrt(_x*_x + _y*_y);
}

point point::rotated(double alpha){
    point b;
    b._x = _x*cos(alpha) - _y*sin(alpha);
    b._y = _x*sin(alpha) + _y*cos(alpha);
    return b;
}

double point::getX(){
    return _x;
}

double point::getY(){
    return _y;
}











