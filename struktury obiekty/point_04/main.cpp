/* point_04
Program w C++ definiujący strukturę point
i metody obsługujące tę strukturę.
Definicje metod znajdują się poza
deklaracją struktury.
*/

#include <iostream>  //obsługa strumieni wejścia/wyjścia
#undef __STRICT_ANSI__
#include <cmath>     //biblioteka matematyczna

using namespace std;

struct point{
    double x,y;

    void print();
    double modulus();
    point rotated(double alpha);
    double getX();
    double getY();
};

int main()
{
    point a = {3,4}, b;
    cout << "a = "; a.print(); cout << endl;
    cout << "a_x = " << a.getX() << endl;
    cout << "a_y = " << a.getY() << endl;
    cout << "|a| = " << a.modulus() << endl;
    b = a.rotated(M_PI_2);
    cout << "b = "; b.print(); cout << endl;
    return 0;
}

//definicje metod obsługujących strukturę point
void point::print(){
    cout << "[" << x << "," << y << "]";
}

double point::modulus(){
    return sqrt(x*x + y*y);
}

point point::rotated(double alpha){
    point b;
    b.x = x*cos(alpha) - y*sin(alpha);
    b.y = x*sin(alpha) + y*cos(alpha);
    return b;
}

double point::getX(){
    return x;
}

double point::getY(){
    return y;
}











