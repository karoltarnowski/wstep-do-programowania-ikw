/* point_03
Program w C++ definiujący strukturę point
i metody należące do tej struktury.
*/

#include <iostream>  //obsługa strumieni wejścia/wyjścia
#undef __STRICT_ANSI__
#include <cmath>     //biblioteka matematyczna

using namespace std;

struct point{
    double x,y;

    //definicje metod obsługujących strukturę point
    void print(){
        //wypisanie współrzędnych punktu do strumienia cout
        cout << "[" << x << "," << y << "]";
    }

    double modulus(){
        return sqrt(x*x + y*y);
    }

    point rotated(double alpha){
        point b;
        b.x = x*cos(alpha) - y*sin(alpha);
        b.y = x*sin(alpha) + y*cos(alpha);
        return b;
    }

    double getX(){
        return x;
    }

    double getY(){
        return y;
    }
};

int main()
{
    //dostęp do metod obsługujących strukturę
    //uzyskuje się analogicznie jak dostęp do
    //pól struktury - operatorem .
    point a = {3,4}, b;
    cout << "a = "; a.print(); cout << endl;
    cout << "a_x = " << a.getX() << endl;
    cout << "a_y = " << a.getY() << endl;
    cout << "|a| = " << a.modulus() << endl;
    b = a.rotated(M_PI_2);
    cout << "b = "; b.print(); cout << endl;
    return 0;
}




















