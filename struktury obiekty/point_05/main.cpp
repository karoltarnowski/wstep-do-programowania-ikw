/* point_05
Program w C++ definiujący strukturę point
i metody obsługujące tę strukturę.
Definicje metod znajdują się poza
deklaracją struktury.
*/

#include <iostream>  //obsługa strumieni wejścia/wyjścia
#undef __STRICT_ANSI__
#include <cmath>
#include "point.h"

using namespace std;

int main()
{
    point a = {3,4}, b;
    cout << "a = "; a.print(); cout << endl;
    cout << "a_x = " << a.getX() << endl;
    cout << "a_y = " << a.getY() << endl;
    cout << "|a| = " << a.modulus() << endl;
    b = a.rotated(M_PI_2);
    cout << "b = "; b.print(); cout << endl;
    return 0;
}












