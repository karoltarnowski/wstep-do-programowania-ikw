#ifndef POINT_H_INCLUDED
#define POINT_H_INCLUDED

struct point{
    double x,y;

    void print();
    double modulus();
    point rotated(double alpha);
    double getX();
    double getY();
};

#endif // POINT_H_INCLUDED
