#include <iostream>
#include <cmath>
#include "point.h"

using namespace std;

//definicje metod obsługujących strukturę point
void point::print(){
    cout << "[" << x << "," << y << "]";
}

double point::modulus(){
    return sqrt(x*x + y*y);
}

point point::rotated(double alpha){
    point b;
    b.x = x*cos(alpha) - y*sin(alpha);
    b.y = x*sin(alpha) + y*cos(alpha);
    return b;
}

double point::getX(){
    return x;
}

double point::getY(){
    return y;
}
