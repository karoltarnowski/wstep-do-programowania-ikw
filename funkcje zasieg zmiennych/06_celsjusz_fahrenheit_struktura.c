/*06_celsjusz_fahrenheit_struktura.c*/
/*Przeliczanie temperatury
  ze stopni Celsjusza
  na stopnie Fahrenheita.
  Program z wydzielonymi czesciami:
  - odczyt danych wejsciowych,
  - obliczenia,
  - wyswietlenie wyniku.*/
#include<stdio.h>

//deklaracja funkcji
float cels2fahr(float);
float readCels();
void printResult(float, float);

int main(){
   //deklaracja zmiennej
   float celsjusz;

   //wczytanie danych wejsciowych
   celsjusz = readCels();

   //wykonanie obliczen i wyswietlenie wyniku
   printResult(celsjusz, cels2fahr(celsjusz));

   return 0;
}

//definicja funkcji
float cels2fahr(float c){
   return 32 + 1.8*c;
}

float readCels(){
   float c;
   printf("Podaj temperature w Celsjuszach: ");
   scanf("%f",&c);
   return c;
}

void printResult(float c, float f){
   printf("Temperatura %.2f stopni Celsjusza\n",c);
   printf("odpowiada temperaturze %.2f stopni Fahrenheita.", f);
   return;
}
