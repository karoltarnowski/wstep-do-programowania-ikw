/*02_celsjusz_fahrenheit_struktura.c*/
/*Przeliczanie temperatury
  ze stopni Celsjusza
  na stopnie Fahrenheita.
  Struktura programu.*/
#include<stdio.h>

main(){
   //deklaracja zmiennych
   float celsjusz, fahrenheit;

   //wczytanie danych wejsciowych
   printf("Podaj temperature w Celsjuszach: ");
   scanf("%f",&celsjusz);

   //wykonanie obliczen
   fahrenheit = 32 + 1.8*celsjusz;

   //wyswietlenie wyniku
   printf("Temperatura %.2f stopni Celsjusza\n",celsjusz);
   printf("odpowiada temperaturze %.2f stopni Fahrenheita.", fahrenheit);

   return 0;
}

