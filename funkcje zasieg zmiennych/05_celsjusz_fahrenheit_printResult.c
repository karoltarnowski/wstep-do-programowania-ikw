/*05_celsjusz_fahrenheit.c*/
/*Przeliczanie temperatury
  ze stopni Celsjusza
  na stopnie Fahrenheita.
  Funkcje cels2fahr(),
  readCels() oraz printResult().*/
#include<stdio.h>

//deklaracje funkcji
float cels2fahr(float);
float readCels();
void printResult(float, float);

main(){
   //deklaracja zmiennych
   float celsjusz, fahrenheit;

   //wczytanie danych wejsciowych
   celsjusz = readCels();

   //wykonanie obliczen
   fahrenheit = cels2fahr(celsjusz); //wywolanie funkcji

   //wyswietlenie wyniku
   printResult(celsjusz, fahrenheit);

   return 0;
}

//definicja funkcji
float cels2fahr(float c){
   return 32 + 1.8*c;
}

float readCels(){
   float c;
   printf("Podaj temperature w Celsjuszach: ");
   scanf("%f",&c);
   return c;
}

void printResult(float c, float f){
   printf("Temperatura %.2f stopni Celsjusza\n",c);
   printf("odpowiada temperaturze %.2f stopni Fahrenheita.", f);
   return;
}
