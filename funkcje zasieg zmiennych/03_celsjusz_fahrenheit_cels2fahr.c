/*03_celsjusz_fahrenheit_cels2fahr.c*/
/*Przeliczanie temperatury
  ze stopni Celsjusza
  na stopnie Fahrenheita.
  Funkcja cels2fahr().*/
#include<stdio.h>

//deklaracja funkcji
float cels2fahr(float);

main(){
   //deklaracja zmiennych
   float celsjusz, fahrenheit;

   //wczytanie danych wejsciowych
   printf("Podaj temperature w Celsjuszach: ");
   scanf("%f",&celsjusz);

   //wykonanie obliczen
   fahrenheit = cels2fahr(celsjusz); //wywolanie funkcji

   //wyswietlenie wyniku
   printf("Temperatura %.2f stopni Celsjusza\n",celsjusz);
   printf("odpowiada temperaturze %.2f stopni Fahrenheita.", fahrenheit);

   return 0;
}

//definicja funkcji
float cels2fahr(float c){
   return 32 + 1.8*c;
}
