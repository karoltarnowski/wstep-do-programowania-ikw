/*08_celsjusz_fahrenheit_zmienne_globalne.c*/
/*Przeliczanie temperatury
  ze stopni Celsjusza
  na stopnie Fahrenheita.
  Zmienne globalne.*/
#include<stdio.h>

float celsjusz, fahrenheit;

void cels2fahr();
void readCels();
void printResult();

int main(){
   readCels();
   cels2fahr();
   printResult();
   return 0;
}

//funkcja ma dostep do zmiennej globalnej celsjusz
void cels2fahr(){
   fahrenheit = 32 + 1.8*celsjusz;
}

//funkcja ma dostep do zmiennej globalnej celsjusz
void readCels(){
   printf("Podaj temperature w Celsjuszach: ");
   scanf("%f",&celsjusz);
}

//funkcja ma dostep do zmiennych globalnych
//celsjusz i fahrenheit
void printResult(){
   printf("Temperatura %.2f stopni Celsjusza\n",celsjusz);
   printf("odpowiada temperaturze %.2f stopni Fahrenheita.", fahrenheit);
   return;
}
