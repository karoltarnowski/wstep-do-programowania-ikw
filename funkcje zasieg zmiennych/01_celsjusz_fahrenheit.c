/*01_celsjusz_fahrenheit.c*/
/*Przeliczanie temperatury
  ze stopni Celsjusza
  na stopnie Fahrenheita.*/
#include<stdio.h>
main(){
   float celsjusz, fahrenheit;
   printf("Podaj temperature w Celsjuszach: ");
   scanf("%f",&celsjusz);
   fahrenheit = 32 + 1.8*celsjusz;
   printf("Temperatura %.2f stopni Celsjusza\n",celsjusz);
   printf("odpowiada temperaturze %.2f stopni Fahrenheita.", fahrenheit);
   return 0;
}
