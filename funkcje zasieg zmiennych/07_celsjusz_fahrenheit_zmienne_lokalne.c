/*07_celsjusz_fahrenheit_zmienne_lokalne.c*/
/*Przeliczanie temperatury
  ze stopni Celsjusza
  na stopnie Fahrenheita.
  Zmienne lokalne.*/
#include<stdio.h>

float cels2fahr(float);
float readCels();
void printResult(float, float);

int main(){
   //zasięg zmiennej celsjusz ogranicza się do funkcji main()
   float celsjusz;
   celsjusz = readCels();
   printResult(celsjusz, cels2fahr(celsjusz));
   return 0;
}

float cels2fahr(float c){
   //zmienna automatyczna c
   //zasięg zmiennej ogranicza się do funkcji cels2fahr()
   return 32 + 1.8*c;
}

float readCels(){
   //zasięg zmiennej c ogranicza się do funkcji readCels()
   float c;
   printf("Podaj temperature w Celsjuszach: ");
   scanf("%f",&c);
   return c;
}

void printResult(float c, float f){
   //zasięg zmiennych c i f ogranicza się do funkcji printResult()
   printf("Temperatura %.2f stopni Celsjusza\n",c);
   printf("odpowiada temperaturze %.2f stopni Fahrenheita.", f);
   return;
}














