/*04_celsjusz_fahrenheit_readCels.c*/
/*Przeliczanie temperatury
  ze stopni Celsjusza
  na stopnie Fahrenheita.
  Funkcje cels2fahr()
  oraz readCels().*/
#include<stdio.h>

//deklaracje funkcji
float cels2fahr(float);
float readCels();

main(){
   //deklaracja zmiennych
   float celsjusz, fahrenheit;

   //wczytanie danych wejsciowych
   celsjusz = readCels(); //wywolanie funkcji

   //wykonanie obliczen
   fahrenheit = cels2fahr(celsjusz); //wywolanie funkcji

   //wyswietlenie wyniku
   printf("Temperatura %.2f stopni Celsjusza\n",celsjusz);
   printf("odpowiada temperaturze %.2f stopni Fahrenheita.", fahrenheit);

   return 0;
}

//definicja funkcji
float cels2fahr(float c){
   return 32 + 1.8*c;
}

//funkcja wczytywania danych
float readCels(){
   float c;
   printf("Podaj temperature w Celsjuszach: ");
   scanf("%f",&c);
   return c;
}
