//reprezentacja_algorytmu.c

#include <stdio.h>

int main()
{
    float x;
    printf("Podaj x:"); scanf("%f",&x);

    if(x>0)
        printf("f(x) = 1");
    else //w tym przypadku x <= 0
        if(x==0)
            printf("f(x) = 0");
        else //w tym przypadku x < 0
            printf("f(x) = -1");
    return 0;
}
