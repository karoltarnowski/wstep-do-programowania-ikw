/*projekt02*/
/*Program ilustruje podział kodu źródłowego
programu między kilka plików.*/
#include <stdio.h>
#include "sort.h" //dołączono plik nagłówkowy

#define N 10

//kod źródłowy funkcji głównej - pozostaje w głównym pliku programu
int main()
{
    double t[N] = {0.943, 0.464, 0.982, 0.709, 0.898, 0.167, 0.386, 0.770, 0.619, 0.517};
    printf("max: %.3f\n",max(t,N));
    return 0;
}
