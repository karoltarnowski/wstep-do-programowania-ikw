/*kostka_rand.c*/
/*Wstęp do programowania*/
/*Program demonstruje
użycie funkcji rand().*/

#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#define N 6

int main(){
   int kostka;
   char znak;

   printf("Program symuluje rzuty kostka.\n");
   printf("Aby przerwac dzialanie wprowadz znak x.\n");

   do{
      kostka = rand() % N + 1;
      //reszta z dzielenia przez N to liczba od 0 do N-1
      printf("Na kostce wypadlo %d.\n", kostka);
      scanf(" %c",&znak);
   }while(toupper(znak) != 'X');

   return 0;
}























