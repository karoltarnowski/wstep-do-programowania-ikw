/*01_zwracanie_wyniku.c*/
/*Zwracanie wyniku przez funkcje*/

#include <stdio.h>

float max(float, float, float);

int main(){
   float a, b, c;
   a = 3.14; //liczba pi
   b = 2.72; //podstawa logarytmu naturalnego
   c = 4.67; //stala Feigenbauma
   printf("Najwieksza z liczb = %g\n",max(a,b,c));
   return 0;
}

float max(float x, float y, float z){
   if(y>x)
      x = y;
   if(z>x)
      x = z;
   return x; //zwracana jest wartosc zmiennej typu float
}
