/*Wstęp do programowania*/
/*Program demonstruje stałe z pliku
  nagłówkowego float.h*/

#include <stdio.h>
#include <float.h>

int main(){
   char format_int[] = "%14s %12d\n";
   char format_flt[] = "%14s %12g\n";
   printf(format_int,"DBL_RADIX",FLT_RADIX);
   printf(format_int,"DBL_DIG",DBL_DIG);
   printf(format_flt,"DBL_EPSILON",DBL_EPSILON);
   printf(format_int,"DBL_MANT_DIG",DBL_MANT_DIG);
   printf(format_flt,"DBL_MAX",DBL_MAX);
   printf(format_int,"DBL_MAX_EXP",DBL_MAX_EXP);
   printf(format_int,"DBL_MAX_10_EXP",DBL_MAX_10_EXP);
   printf(format_flt,"DBL_MIN",DBL_MIN);
   printf(format_int,"DBL_MIN_EXP",DBL_MIN_EXP);
   printf(format_int,"DBL_MIN_10_EXP",DBL_MIN_10_EXP);
}

