/*Wstęp do programowania*/
/*Program demonstruje stałe z pliku
  nagłówkowego float.h*/

#include <stdio.h>
#include <float.h>

int main(){
   char format_int[] = "%14s %12d\n";
   char format_flt[] = "%14s %12g\n";
   printf(format_int,"FLT_RADIX",FLT_RADIX);
   printf(format_int,"FLT_DIG",FLT_DIG);
   printf(format_flt,"FLT_EPSILON",FLT_EPSILON);
   printf(format_int,"FLT_MANT_DIG",FLT_MANT_DIG);
   printf(format_flt,"FLT_MAX",FLT_MAX);
   printf(format_int,"FLT_MAX_EXP",FLT_MAX_EXP);
   printf(format_int,"FLT_MAX_10_EXP",FLT_MAX_10_EXP);
   printf(format_flt,"FLT_MIN",FLT_MIN);
   printf(format_int,"FLT_MIN_EXP",FLT_MIN_EXP);
   printf(format_int,"FLT_MIN_10_EXP",FLT_MIN_10_EXP);
}
