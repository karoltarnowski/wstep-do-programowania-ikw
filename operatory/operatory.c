/*operatory.c*/
/*Karol Tarnowski*/
/*Wstęp do programowania*/
/*Przykładowy program demonstrujący
  działanie operatorów dzielenia
  i dzielenie modulo.*/
/*na podstawie: G. Perry, D. Miller,
Język C Programowanie dla początkujących, Helion, 2014*/

#include <stdio.h>

main(){
   /*Dwa zestawy równoważnych zmiennych:*/
   /*zmiennopozycjnych*/
   float xf = 22.0;
   float yf =  5.0;
   float wynikFloat;
   /*całkowioliczbowych*/
   int xi = 22;
   int yi = 5;
   int wynikInt;

   //Iloraz dwóch liczb zmiennopozycyjnych wyniesie 4.4
   wynikFloat = xf/yf;
   printf("%.1f podzielone na %.1f wynosi %.1f.\n", xf, yf, wynikFloat);

   //Iloraz dwóch liczb całkowitoliczbowych wyniesie 4
   wynikInt = xi/yi;
   printf("%.1d podzielone na %.1d wynosi %.1d.\n", xi, yi, wynikInt);

   //W tym przypadku zachodzi obcięcie wartości
   wynikInt = xf/yf;
   printf("%.1f podzielone na %.1f wynosi %.1d.\n", xf, yf, wynikInt);

   //Wykorzystując operator modulo (%) można obliczyć resztę
   wynikInt = xi%yi;
   printf("Resza dzielenia %.1d przez %.1d wynosi %.1d.\n", xi, yi, wynikInt);

   return 0;
}








