/*wskazniki.c*/
/*Wstęp do programowania*/
/*Program demonstruje
dostęp do zmiennych
przy użyciu wskaźników.*/

#include <stdio.h>

int main(){
   char znak;
   char* wskZnak;
   int liczba;
   int* wskLiczba;

   /*Program prosi o podanie znaku,
   a następnie go wypisuje.
   Program prosi o podanie liczby całkowitej,
   a następnie ją wypisuje.*/

   printf("Podaj znak: ");
   scanf(" %c",&znak);
   printf("Podany znak to: %c.\n",znak);
   printf("Podaj liczbe calkowita: ");
   scanf("%d",&liczba);
   printf("Podana liczba to: %d.\n",liczba);

   /*Program powtarza działanie
   z wykorzystaniem wskaźników.*/
   wskZnak = &znak;
   wskLiczba = &liczba;
   printf("Podaj znak: ");
   scanf(" %c",wskZnak);
   printf("Podany znak to: %c.\n",*wskZnak);
   printf("Podaj liczbe calkowita: ");
   scanf("%d",wskLiczba);
   printf("Podana liczba to: %d.\n",*wskLiczba);
   return 0;
}



















