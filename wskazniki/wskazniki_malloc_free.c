/*wskazniki_malloc_free.c*/
/*Wstęp do programowania*/
/*Program demonstruje
dynamiczną alokację pamięci.*/

#include <stdio.h>
#include <stdlib.h>

int main(){
   int i, n;
   float *tab;

   printf("Program wczytuje n liczb rzeczywistych\
 i zapisuje je w tablicy.\n");
   printf("Podaj n: ");
   scanf("%d",&n);

   tab = malloc(n*sizeof(*tab));

   for(i=0; i<n; i++){
      printf("Podaj liczbe: ");
      scanf("%f",&tab[i]);
      //scanf("%f",tab+i);
   }

   for(i=0; i<n; i++){
      printf("%d\t%f\n",i,tab[i]);
      //printf("%d\t%f\n",i,*(tab+i));
   }

   free(tab);

   return 0;
}














