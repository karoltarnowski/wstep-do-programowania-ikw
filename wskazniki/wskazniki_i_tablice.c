/*wskazniki_i_tablice.c*/
/*Wstęp do programowania*/
/*Program demonstruje
dostęp do tablicy
przy użyciu wskaźników.*/

#include <stdio.h>

int main(){
   int tab[5] = {10, 20, 30, 40, 50};

   //wypisanie początkowego elementu tablicy
   printf("tab[0]   = %d\n", tab[0]);

   //wypisanie adresu tablicy (jej początku)
   printf("tab      = %d\n", tab);

   //wypisanie początkowego elementu tablicy
   printf("*tab     = %d\n", *tab);

   //wypisanie elementu tablicy o indeksie 2
   printf("tab[2]   = %d\n", tab[2]);
   printf("*(tab+2) = %d\n", *(tab+2));

   //wypisanie adresu elementu tablicy o indeksie 2
   printf("tab+2    = %d\n", tab+2);

   return 0;
}












