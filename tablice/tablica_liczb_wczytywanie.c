/*tablica_liczb_wczytywanie.c*/
/*Wstęp do programowania*/

#include <stdio.h>
#define N 7

int main(){
   int i;
   float liczby[N];
   float suma = 0;

   printf("Program wczytuje %d liczb rzeczywistych do tablicy.\n",N);
   printf("Wyswietla tablice, oraz podaje sume elementow i srednia.\n");

   for(i=0; i<N; i++){
      printf("Podaj %d. liczbe: ",i);
      scanf("%f",&liczby[i]);
   }

   printf("indeks\tliczba\n");

   for(i=0; i<N; i++){
      printf("%d\t%f\n",i,liczby[i]);
      suma += liczby[i];
   }

   printf("Suma = %f,\nsrednia = %f.",suma,suma/N);

   return 0;
}
















