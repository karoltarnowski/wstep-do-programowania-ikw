/*04_przekazywanie_tablicy.c*/
/*Przekazywanie tablicy do funkcji*/

#include <stdio.h>

void zeruj(float [], int);

int main(){
   int i,n = 5;
   //deklaracja i inicjalizacja tablicy tab
   float tab[5] = {2.3,5.7,11.13,17.19,23.27};
   //wyswietlenie tablicy
   for(i=0;i<n;i++)
      printf("tab[%d] = %g\n",i,tab[i]);
   //wywolanie funkcji zeruj()
   //tablica przekazana przez referencje
   zeruj(tab,n);
   //ponowne wyswietlenie tablicy
   for(i=0;i<n;i++)
      printf("tab[%d] = %g\n",i,tab[i]);
   return 0;
}

void zeruj(float t[], int n){
   int i;
   //przejscie przez tablice n-elementowa
   //petla for i wyzerowanie wszystkich elementow
   for(i=0;i<n;i++)
      t[i] = 0;
}
