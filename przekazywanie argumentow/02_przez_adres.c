/*02_przez_adres.c*/
/*Przekazywanie zmiennej
  przez adres.*/

#include <stdio.h>

void half(int * pa);

int main(){
   //deklaracja i inicjalizacja zmiennej a
   int a = 13;
   //wyświetlenie zmiennej a
   printf("(main) a = %d\n",a);
   //zmienna a przekazana do funkcji half() przez adres
   //do funkcji() half przekazywany jest wskaźnik na zmienną
   half(&a);
   //wyświetlenie zmiennej a
   printf("(main) a = %d\n",a);
   return 0;
}

void half(int * pa){
   //zmienna pod adresem pa jest podzielona przez 2
   *pa = *pa/2;
   //wyswietlenie zmiennej wskazywanej przez pa
   printf("(half) a = %d\n",*pa);
}
