/*01_przez_wartosc.c*/
/*Przekazywanie zmiennej
  przez wartosc.*/

#include <stdio.h>

void half(int a);

int main(){
   //deklaracja i inicjalizacja zmiennej a
   int a = 13;
   //wyswietlenie zmiennej a
   printf("(main) a = %d\n",a);
   //zmienna a przekazana do funkcji half() przez wartosc
   half(a);
   //wyswietlenie zmiennej a
   printf("(main) a = %d\n",a);
   return 0;
}

void half(int a){
   //zmienna a podzielona przez 2
   a = a/2;
   //wyswietlenie zmiennej a
   printf("(half) a = %d\n",a);
}
