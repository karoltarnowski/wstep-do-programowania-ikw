/*point.h*/
/*Plik nagłówkowy zawierający
definicję struktury point.*/
#ifndef POINT_H_INCLUDED
#define POINT_H_INCLUDED

//struktura point ma dwa pola
//typu double
struct point {
   double x; //współrzędna x
   double y; //współrzędna y
};

#endif // POINT_H_INCLUDED
