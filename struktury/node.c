/*node.c*/
/*Przykładowy program zawierający definicję
struktury cyklicznej*/
#include <stdlib.h>

struct Node{
   struct Node * ptr;
};

typedef struct Node Node;

int main(){
   Node * ptr1 = malloc(sizeof(Node));
   Node * ptr2 = malloc(sizeof(Node));
   Node * ptr3 = malloc(sizeof(Node));
   ptr1->ptr = ptr2;
   ptr2->ptr = ptr3;
   ptr3->ptr = NULL;
   free(ptr1);
   free(ptr2);
   free(ptr3);
   return 0;
}
