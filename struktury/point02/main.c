/*main.c*/
/*Program demonstruje wykorzystanie
struktury point zdefiniowanej
w pliku nagłówkowym point.h
wraz z funkcjami zdefiniowanymi
w pliku point.c.*/

#include <stdio.h>
#include "point.h"

int main()
{
    struct point a, b;

    //wczytanie współrzędnych punktów
    //z wykorzystaniem funkcji readPoint()
    readPoint(&a);
    readPoint(&b);

    //wyświetlenie współrzędnych punktów
    //z wykorzystaniem funkcji printPoint()
    printf("a = ");
    printPoint(&a);
    printf("\n");
    printf("b = ");
    printPoint(&b);
    printf("\n");

    return 0;
}



















