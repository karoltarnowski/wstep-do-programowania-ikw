/*03_getchar_bufor.c*/
/*Wstep do programowania*/
/*na podstawie: G. Perry, D. Miller,
Jezyk C Programowanie dla poczatkujacych, Helion, 2014*/
/*Program ilustruje problem ze znakiem
nowego wiersza pozostajacym w buforze*/

#include <stdio.h>

main(){
   char inicjalImienia, inicjalNazwiska;

   printf("Podaj inicjal imienia.\n");
   inicjalImienia = getchar();

   printf("Podaj inicjal nazwiska.\n");
   inicjalNazwiska = getchar();

   printf("Twoje inicjaly to %c. %c.\n",inicjalImienia, inicjalNazwiska);

   return 0;
}
