/*01_putchar.c*/
/*Wstep do programowania*/
/*na podstawie: G. Perry, D. Miller,
J�zyk C Programowanie dla poczatkujacych, Helion, 2014*/
/*Program ilustruje wykorzystanie funkcji
putchar() do wypisania napisu*/

#include <stdio.h>
#include <string.h>

main(){
   int i;
   char msg[] = "Programowanie w C jest fajne!";
   for(i=0; i < strlen(msg); i++){
      putchar(msg[i]);
   }
   putchar('\n');
   return 0;
}


