/*02_getchar_putchar.c*/
/*Wstep do programowania*/
/*na podstawie: G. Perry, D. Miller,
Jezyk C Programowanie dla poczatkujacych, Helion, 2014*/
/*Program ilustruje wykorzystanie funkcji
getchar() i putchar()*/

#include <stdio.h>
#define N 25

main(){
   int i;
   char msg[N];

   printf("Wpisz maksymalnie 25 znakow i nacisnij klawisz Enter...\n");

   for(i = 0; i < N; i++){
      msg[i] = getchar(); //pobranie pojedynczego znaku
      if(msg[i] == '\n'){ //jesli pobrany znak jest znakiem nowej linii
         break;           //zakoncz wczytywanie
      }
   }
   i--;

   putchar('\n');         //dodatkowe przelamanie linii

   //wypisanie napisu wspak
   for( ; i >= 0; i--){   //poczatkowa wartosc zmiennej i
      putchar(msg[i]);    // zostala ustalona w poprzedniej petli
   }

   putchar('\n');

   return 0;
}















