/*08_string.c*/
/*Wstep do programowania*/
/*Program demostruje funkcje
z biblioteki string*/

#include <stdio.h>
#include <string.h>

main(){
   char napis1[14] = "Pro";
   char napis2[]   = "gra";
   char napis3[]   = "mowa";
   char napis4[]   = "nie";
   char napis5[5];

   /*funkcja strcpy()
   kopiujemy napis3 do napis5*/
   strcpy(napis5, napis3);
   printf(napis5);
   printf("\n");

   /*funkcja strlen()
   zwraca dlugosc napisu
   liczba znak�w do znaku '\0'*/
   printf("napis1: %s ",napis1);
   printf("ma dlugosc %d\n",strlen(napis1));

   /*funkcja strcat
   dolacza (konkatenuje)
   dwa lancuchy
   (w pierwszym musi byc
    dostatecznie duzo miejsca*/
   strcat(napis1, napis2);
   printf("napis1: %s ",napis1);
   printf("ma dlugosc %d\n",strlen(napis1));

   strcat(napis1, napis3);
   strcat(napis1, napis4);
   printf("napis1: %s ",napis1);
   printf("ma dlugosc %d\n",strlen(napis1));

   return 0;
}

















