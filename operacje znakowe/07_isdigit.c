/*07_isdigit.c*/
/*Wstep do programowania*/
/*Program sprawdza,
czy wprowadzony znak jest cyfra*/

#include <stdio.h>
#include <ctype.h>

main(){
   char znak;
   printf("Wpisz znak: ");
   znak = getchar();

   if( isdigit(znak) )
      printf("Wpisany znak jest cyfra.\n");
   else{
      printf("Wpisany znak nie jest cyfra.\n");
   }

   return 0;
}
