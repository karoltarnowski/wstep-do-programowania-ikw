/*Wstęp do programowania*/
/*Program demonstruje stale z pliku
  nagłówkowego limits.h*/

#include <stdio.h>
#include <limits.h>

int main(){
   printf("%10s %21d\n","CHAR_BIT",CHAR_BIT);
   printf("%10s %21d\n","CHAR_MIN",CHAR_MIN);
   printf("%10s %21d\n","CHAR_MAX",CHAR_MAX);
   printf("%10s %21d\n","INT_MIN", INT_MIN);
   printf("%10s %21d\n","INT_MAX", INT_MAX);
   printf("%10s %21ld\n","LONG_MIN",LONG_MIN);
   printf("%10s %21ld\n","LONG_MAX",LONG_MAX);
   printf("%10s %21lld\n","LLONG_MIN",LLONG_MIN);
   printf("%10s %21lld\n","LLONG_MAX",LLONG_MAX);
   printf("%10s %21d\n","SCHAR_MIN",SCHAR_MIN);
   printf("%10s %21d\n","SCHAR_MAX",SCHAR_MAX);
   printf("%10s %21d\n","SHRT_MIN",SHRT_MIN);
   printf("%10s %21d\n","SHRT_MAX",SHRT_MAX);
   printf("%10s %21d\n","UCHAR_MAX",UCHAR_MAX);
   printf("%10s %21u\n","UINT_MAX",UINT_MAX);
   printf("%10s %21lu\n","ULONG_MAX",ULONG_MAX);
   printf("%10s %21d\n","USHRT_MAX",USHRT_MAX);
}
