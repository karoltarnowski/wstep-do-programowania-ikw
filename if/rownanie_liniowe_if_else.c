/*rownanie_liniowe_if_else.c*/
/*Karol Tarnowski*/
/*Wstęp do programowania*/
/*Program rozwiązujący równanie liniowe
(wykorzystuje instrukcję if...else)*/

#include <stdio.h>

main(){
   float a, b;

   printf("Program rozwiazuje rownanie a*x + b = 0\n");
   printf("dla podanych wspolczynnikow a oraz b.\n\n");

   //wczytywanie danych
   printf("Podaj a: ");
   scanf("%f",&a);
   printf("Podaj b: ");
   scanf("%f",&b);

   //jeśli a == 0
   if(a == 0){
      printf("Niestety nie potrafie rozwiazac rownania dla a = 0.\n");
   }
   //jeśli a != 0
   else{
      //obliczenie wyniku i wyświetlenie rozwiązania
      printf("Rozwiazaniem jest x = %g\n",-b/a);
   }
   return 0;
}
















