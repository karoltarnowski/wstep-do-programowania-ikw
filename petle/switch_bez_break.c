/*switch_bez_break.c*/
/*Wstęp do programowania*/
/*Program ilustruje użycie instrukcji switch*/
#include <stdio.h>

main(){
   int choice;

   printf("Mozesz wybrac jedna opcje.\n");
   printf("1. Pierwsza.\n");
   printf("2. Druga.\n");
   printf("3. Trzecia.\n");
   printf("4. Czwarta. \n");
   printf("Wybierz opcje:\n");
   scanf("%d",&choice);

   switch(choice){
      case(1):
         printf("Wybrales opcje pierwsza.\n");
      case(2):
         printf("Wybrales opcje druga.\n");
      case(3):
         printf("Wybrales opcje trzecia.\n");
      case(4):
         printf("Wybrales opcje czwarta.\n");
      default:
         printf("Nie ma takiej opcji.\n");
   }

   return 0;
}


















