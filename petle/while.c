/*while.c*/
/*Wstęp do programowania*/
/*na podstawie: G. Perry, D. Miller,
Język C Programowanie dla początkujących, Helion, 2014*/

#include <stdio.h>

main(){
   int licznik = 0;

   while(licznik < 4){
      printf("Licznik ma wartosc %d.\n",++licznik);
   }
   while(licznik > 1){
      printf("Licznik ma wartosc %d.\n",--licznik);
   }

   return 0;
}







