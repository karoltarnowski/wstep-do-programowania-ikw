/*do_while.c*/
/*Wstęp do programowania*/
/*na podstawie: G. Perry, D. Miller,
Język C Programowanie dla początkujących, Helion, 2014*/

#include <stdio.h>

main(){
   int licznik = 0;

   do{
      printf("Licznik ma wartosc %d.\n",++licznik);
   }while(licznik < 4);

   do{
      printf("Licznik ma wartosc %d.\n",--licznik);
   }while(licznik > 1);

   return 0;
}







