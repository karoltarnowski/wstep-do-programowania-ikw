/*rekurencja.c*/
/*Program demonstruje działanie rekurencji
 na przykładzie funkcji obliczającej
 silnię. Pokazano także wykorzystanie
 rekurencji ogonowej.*/

#include <stdio.h>

int silnia_rek(int n);
int silnia_ogon(int n);
int silnia_pom(int n, int a);

int main(){
    int n = 5;
    char format[] = "%d! = %d\n";
    printf(format,n,silnia_rek(n));
    printf(format,n,silnia_ogon(n));
    return 0;
}
int silnia_rek(int n){
    if(n==0)
        return 1;
    else
        return n*silnia_rek(n-1);
}

int silnia_ogon(int n){
    return silnia_pom(n,1);
}

int silnia_pom(int n, int a){
    if(n==0)
        return a;
    else
        return silnia_pom(n-1,a*n);
}




