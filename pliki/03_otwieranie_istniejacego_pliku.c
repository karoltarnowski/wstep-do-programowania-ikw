/*03_otwieranie_istniejacego_pliku.c*/
/*Wstep do programowania*/
/*Dostep do pliku.
Otwarcie do odczytu
istniejacego pliku.*/
#include <stdio.h>

int main()
{
    FILE* fptr;
    fptr = fopen("dane.txt","r");
    printf("%d",(int)fptr);
    /*wlasciwa czesc programu*/
    fclose(fptr);
    return 0;
}
