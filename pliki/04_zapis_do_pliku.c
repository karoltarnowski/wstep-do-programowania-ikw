/*04_zapis_do_pliku.c*/
/*Wstep do programowania*/
/*Zapis danych do pliku.*/
#include <stdio.h>

int main()
{
    //deklaracja trzech zmiennych
    double a, b, c;
    //i wskaznika do pliku
    FILE* fptr;

    //wczytanie wartosci zmiennych
    //ze standardowego wejscia
    printf("Podaj a: ");
    scanf("%lf",&a);
    printf("Podaj b: ");
    scanf("%lf",&b);
    printf("Podaj c: ");
    scanf("%lf",&c);

    //otworzenie pliku do zapisu
    fptr = fopen("abc.txt","w");
    //zapis danych sformatowanych do pliku
    //(tylko jesli plik zostal poprawnie otwarty)
    if( fptr != NULL ){
        fprintf(fptr,"a = %g\n",a);
        fprintf(fptr,"b = %g\n",b);
        fprintf(fptr,"c = %g\n",c);
        fclose(fptr);
    }
    return 0;
}















