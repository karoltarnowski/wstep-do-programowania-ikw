/*07_linia_polecen.c*/
/*Wstep do programowania*/
/*Program wypisuje zawartosc plikow
wymienionych w linii polecen na stdout.
Zawiera funkcje do kopiowania plikow.
Ilustruje wykorzystanie argumentow
linii polecen*/

#include<stdio.h>

void filecopy(FILE*, FILE*);

int main(int argc, char* argv[])
{
    //deklaracja wskaznika do pliku
    FILE *fp;

    //jezeli jest jeden argument linii polecen
    //(nazwa programu)
    if(argc == 1)
    {
        //kopiowane jest standardowe wejscie
        filecopy(stdin, stdout);
    }

    //jesli sa jakies argumenty
    else{
        //przejdz po wszystkich argumentach
        while(--argc > 0)
        {
            //otworz plik (nazwa pliku jest wskazywana przez kolejny element
            //w tablicy wskaznikow argv
            //w razie niepowodzenia
            if( (fp = fopen(*++argv, "r") ) == NULL )
            {
                //wyswietl komunikat i zakoncz program
                printf("nie mozna otworzyc %s\n", *argv);
                return 1;
            }
            //jesli udalo sie otworzyc
            else
            {
                //skopiuj plik fp na stdout
                filecopy(fp, stdout);
                //zamknij plik
                fclose(fp);
            }
        }
    }

    return 0;
}

//funkcja kopiujaca plik ifp (input)
//do pliku ofp (output)
void filecopy(FILE *ifp, FILE *ofp)
{
    int c;
    //dopoki ( (znak wczytany z wejscia) jest rozny od EOF )
    //wypisuj ten znak na wyjscie
    while( (c = getc(ifp)) != EOF )
        putc(c,ofp);
}















