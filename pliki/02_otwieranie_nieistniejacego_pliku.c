/*02_otwieranie_nieistniejacego_pliku.c*/
/*Wstep do programowania*/
/*Dostep do pliku.
Otwarcie do odczytu
nieistniejacego pliku.*/
#include <stdio.h>

int main()
{
    FILE* fptr;
    fptr = fopen("danetxt","r");
    printf("%d",(int)fptr);
    /*wlasciwa czesc programu*/
    fclose(fptr);
    return 0;
}
