/*05_odczyt_danych_z_pliku.c*/
/*Wstep do programowania*/
/*Odczyt danych z pliku.*/
#include <stdio.h>

int main()
{
    //deklaracja zmiennych
    //i wskaznika do pliku
    double a, b, c;
    FILE* fptr;

    //otworzenie pliku do odczytu
    fptr = fopen("abc.txt","r");

    //jesli otwarto plik
    if( fptr != NULL ){
        //wczytaj dane
        fscanf(fptr,"a = %lf\n",&a);
        fscanf(fptr,"b = %lf\n",&b);
        fscanf(fptr,"c = %lf\n",&c);
        fclose(fptr);
    }

    //wypisz dane na ekran
    printf("a = %g\n",a);
    printf("b = %g\n",b);
    printf("c = %g\n",c);

    return 0;
}









